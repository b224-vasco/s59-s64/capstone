import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Dungeons and Dragons!</Card.Title>
				       <Card.Text>
				         Here we have all the latest editions, modules, and supplements for Dungoens and Dragons.
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Magic: The Gathering!</Card.Title>
				       <Card.Text>
				         All of the MTG expansions, boosters, and singles available here!
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Table Top Board Games!</Card.Title>
				       <Card.Text>
				         We have a myriad of available boardgames in our store! From classic titles to more recent ones.
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

		</Row>
	)
}