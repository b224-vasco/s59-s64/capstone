import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';

export default function Home() {

	const data = {
		title: "Bag of Holding Store",
		content: "For all of your adventuring needs.",
		// destination: "/products",
		// label: "See our products!"
	}

	return (
		<>
			<Banner data={data}/>
			<Highlights/>
			{/*<CourseCard/>*/}
		</>
	)
}