import { useState, useEffect} from 'react';
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import productData from '../data/productData';

export default function AdminTable() {
	const [products, setProducts] = useState([]);

	// Checks to see if the mock data was captured.
	// console.log(coursesData);
	// console.log(coursesData[0]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/getallprod`, {
				headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProducts(data) 
			// console.log(products)
		})
	}, [])

	const archiveProduct = (productId) => {
		console.log("wassup")
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === false){
				Swal.fire({
					title: "Duplicate Product Found",
					icon: "error"
				})
			} else {
				Swal.fire({
					title: "Product successfully archived",
					icon: "success"
				})
			}

			
		})
	}

	const activateProduct = (productId) => {
		console.log("wassup")
		fetch(`${process.env.REACT_APP_API_URL}/products/activate/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === false){
				Swal.fire({
					title: "Duplicate Product Found",
					icon: "error"
				})
			} else {
				Swal.fire({
					title: "Product successfully activated",
					icon: "success"
				})
			}

			
		})
	}
	
	

	return (


		<>
			<div class = "text-center">
				<h1>Admin Dashboard</h1>
				<Link as={Link} to="/createproduct">
					<Button className="bg-primary">Add New Product</Button>
				</Link>
				<Button className="bg-success">Show User Orders</Button>
			</div>
	
			<table class="table table-dark table-striped">
			<thead>
			    <tr>
			      <th scope="col">Name</th>
			      <th scope="col" colspan="3">Description</th>
			      <th scope="col">Price</th>
			      <th scope="col">Availability</th>
			      <th scope="col">Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			  	{products.map((data) => {
			  		console.log(data._id)
			  		return(
			  			<tr>
			  			  <th scope="row">{data.name}</th>
			  			  <td colspan="3">{data.description}</td>
			  			  <td>{data.price}</td>
			  			  <td>{data.isActive ? "Available" : "Not Available"}</td>
			  			  <td>
			  			  	<Link as={Link} to={`/updateproduct/${data._id}`}>
			  			  		<Button className="bg-primary">Update</Button>
			  			  	</Link>
			  			  	{
			  			  		(data.isActive === true) ?
			  			  		<Button onClick={(e) => {archiveProduct(data._id)}} className="bg-danger">Disable</Button>
			  			  		:
			  			  		<Button onClick={(e) => {activateProduct(data._id)}} className="bg-danger">Enable</Button>
			  			  	}
			  			  </td>
			  			</tr>			  		
			  		)
			  	})}
			  </tbody>
			</table>
		</>
	)

}