// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

import Products from './pages/Products';
import ProductsView from './pages/ProductsView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminTable from './pages/AdminTable';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
});

  const unsetUser = () => {
    localStorage.clear();
  }

/*  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

        if(typeof data._id !== "undefined"){

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } else {
            setUser({
                id: null,
                isAdmin: null
            })
        }
    })

  }, [])*/

  return (
    
    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element = {<Home/>} />
                    <Route path="/products" element = {<Products/>} />
                    <Route path="/products/:productId" element = {<ProductsView/>} />
                    <Route path="/register"element = {<Register/>}/>
                    <Route path="/login" element = {<Login/>}/>
                    <Route path="/logout" element = {<Logout/>}/>
                    <Route path="/admintable" element = {<AdminTable/>}/>
                    <Route path="/createproduct" element = {<CreateProduct/>}/>
                    <Route path="/updateproduct/:productId" element = {<UpdateProduct/>}/>
                    <Route path="/*" element = {<Error/>}/>
                </Routes>
            </Container>
        </Router>
    </UserProvider>

  );
}

export default App;